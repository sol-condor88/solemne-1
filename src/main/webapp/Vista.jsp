<%-- 
    Document   : Vista
    Created on : 04-05-2021, 21:40:22
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h3>Solemne 1</h3>
        
        <form name="form" action="ControllerServlet" method="GET">
            <label for="capital">Ingrese capital:</label>
            <input type="text" id="capital" name="cap"><br>
            <br>
            <label for="tasa_interes">Ingrese tasa de interes:</label>
            <input type="text" id="tasa_interes" name="tasain"><br>
            <br>
            <label for="años">Ingrese cantidad de años:</label>
            <input type="text" id="años" name="years"><br>
            <br>
            
            <button type="submit" class="tbn btn-success">Calcular total</button>
        </form>
    </body>
</html>
