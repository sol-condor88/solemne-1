
package cl.Model;


public class Calculadora {
    private int capital;
    private int tasa_interes;
    private int años;
    private int interes_producido;

    
    public int getCapital() {
        return capital;
    }

    
    public void setCapital(int capital) {
        this.capital = capital;
    }

   
    public int getTasa_interes() {
        return tasa_interes;
    }

  
    public void setTasa_interes(int tasa_interes) {
        this.tasa_interes = tasa_interes;
    }

   
    public int getAños() {
        return años;
    }

  
    public void setAños(int años) {
        this.años = años;
    }

    
    public int getInteres_producido() {
        
        int I = this.capital * (this.tasa_interes/100) * this.años;
        return I;
    }

   
    public void setInteres_producido(int interes_producido) {
        this.interes_producido = interes_producido;
    }
}
